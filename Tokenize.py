import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import collections
import numpy as np
import os

from tqdm.auto import tqdm

import re
from sys import argv, stdout, stderr, exit


try:
    path_prefix = argv[1]
    path = path_prefix + '.sentences.train'
except IndexError:
    print(f"usage: {argv[0]} [training path prefix]")
    exit(1)


bis2id = {
        'B': 0,
        'I': 1,
        'S': 2,
}

id2bis = {
        0: 'B',
        1: 'I',
        2: 'S',
}

def printerr(*args):
    ''' Print info stuff to stderr to leave the script's output untouched '''
    print(' '.join(map(str,args)), file=stderr)


def build_vocabulary(path, vocab_size, unk_token='\0'):
    """Defines the vocabulary to be used. Builds a mapping (char, index) for
    each char in the vocabulary. Largely inspired by the word2vec notebook.

    Args:
      data_lines (list[str]): list of text lines
      vocab_size (int): size of the vocabolary
      unk_token (str): token to associate with unknown chars
    Returns:
        char2id (dict): mapping (char -> int) where each key,value pair is unique
    """
    # Get lines of text from the file as list of characters
    with open(path,'r') as f:
        data_lines = (
            (c for c in line.rstrip('\n'))
            for line in f.readlines())

    # count occurrences for each character
    counter_list = []
    for line in data_lines:
        counter_list.extend(line)
    counter = collections.Counter(counter_list)
    counter_len = len(counter)

    # consider only the (vocab size -1) most common chars to build the vocab
    dictionary = {
            key: index
            for index, (key, _)
            in enumerate(counter.most_common(vocab_size - 1))
    }

    assert unk_token not in dictionary

    # all the other chars are mapped to UNK
    dictionary[unk_token] = vocab_size - 1

    # (character -> numerical id) mapping
    char2id = dictionary

    return char2id


def splitrange(N, worker_info=None):
    ''' Necessary for parallel data loading, used in the __iter__ method of dataset.
    Partitions a range of N indexes for use by worker_info.num_workers parallel threads.
    '''
    if worker_info is None:
        # not multiprocessing, use normal range function
        return range(N)
    # get total n. of workers and ID of current thread
    num_workers = worker_info.num_workers
    worker_id = worker_info.id
    # compute portion of indexes for each worker
    per_worker = int(ceil(N / float(num_workers)))
    # start and stop indices for this worker
    start = worker_id * per_worker
    stop = min(start + per_worker, N)
    return range(start, stop)


class BonusExDataset(torch.utils.data.IterableDataset):

    def __init__(self, char2id, txt_path, window_size, unk_token='\0', testing=False):
        """
        Args:
          char2id (dict): mapping (char -> id), obtained from build_vocab
          txt_path (str): Path to the raw-text file.
          window_size (int): Number of chars to consider as context.
          unk_token (str): How will unknown chars represented (e.g. 'UNK').
          testing (bool): tells whether ground truth is going to be available
        """

        self.char2id = char2id
        self.id2char = { value: key for key,value in char2id.items() }
        self.vocab_size = len(char2id.keys())

        self.unk_token = unk_token
        self.testing = testing

        if not testing:
            # ground truth file
            txt_target_path = txt_path.replace('sentences','gold')
            # bis-encoded (target) lines
            self.bis_lines = self.read_data(txt_target_path)

        # number of chars to left and right of input char
        self.window_size = window_size
        self.data_lines = self.read_data(txt_path)

    def __iter__(self):
        ''' Returns one instance of the dataset, as a pair of input/target
        values. Inputs is a window of characters, target is the BIS encoding
        of one of them (the central one most of the time, except when at the
        edges of the line. '''
        lines = self.data_lines
        unk_id = self.char2id[self.unk_token]
        testing = self.testing

        if testing:
            # there's probably a smarter way to get around the problem this line solves
            self.bis_lines = lines
        # for each line of text in input and target file
        for line, bis_line in zip(lines, self.bis_lines):
            len_line = len(line)

            # deallocate unused variable
            if testing: del bis_line
            # this must hold, so we can use same indexes on line and bis_line
            assert testing or len_line == len(bis_line)

            # for each character in the line
            for input_idx in splitrange(len(line)):
                # creation of window start/stop indices
                min_idx = max(0, input_idx - self.window_size)
                max_idx = min(len_line, input_idx + self.window_size)

                # creation of window indices
                window_idxs = (x for x in range(min_idx, max_idx))

                # map window indices to chars, UNK if char not in char2id
                input_chars = (
                        self.char2id.get(line[idx], unk_id)
                        for idx in window_idxs )

                # one-hot encode the characters
                inputs = torch.zeros(self.window_size * 2, self.vocab_size)
                for i, char_id in enumerate(input_chars):
                    inputs[i, char_id] = 1
                # reshaping to concatenate the one-hot vects
                inputs = inputs.reshape(1,-1)

                if not testing:
                    # BIS-encoded central character
                    target = bis2id[bis_line[input_idx]]
                else:
                    target = None

                yield { 'inputs': inputs,
                        'targets': target,
                        'islast': input_idx == len_line-1 # used to print \n during testing
                }



    @property # compute only once
    def _len(self):
        return sum((len(l) for l in self.data_lines))

    def __len__(self):
        return self._len


    def tokenize_line(self, line):
        return [c for c in line.rstrip('\n')]

    def read_data(self, txt_path):
        """Converts each line in the input file into a list of lists of tokenized chars."""
        data = []
        with open(txt_path) as f:
            for line in f:
                split = self.tokenize_line(line)
                if split:
                    data.append(split)
        return data



class BisModule(nn.Module):
    ''' Model with a single hidden layer of size embedding_dim.
    Receives in input a window_size number of characters, and has to predict
    the BIS-encoding of the central one. Outputs a 3-dimensional vector of
    probabilities, corresponding to the B, I and S tags.
    Example:
Input:  ['C', 'a', 'u', 'c', 'h' ] (window size of 2)
                    ^ (<--- central character)
                    |
                    v
         [ hidden representation]
                    |
                    v
Output:           [ I ]
(each char in the input is one hot encoded, so input is a k-hot vector,
where k is double the window size)
    '''

    def __init__(self, vocabulary_size, embedding_dim, window_size):
        super(BisModule, self).__init__()
        self.vocabulary_size = vocabulary_size
        self.embedding_dim = embedding_dim
        self.input_size = vocabulary_size * window_size * 2

        ### LAYERS
        # Input to hidden representation
        self.embeddings = nn.Linear(self.input_size, self.embedding_dim)
        # Hidden to 3 output dimensions: B, I and S
        self.output_weights = nn.Linear(self.embedding_dim, 3)
        # also takes care of softmax during training
        self.loss_function = nn.CrossEntropyLoss()

    def forward(self, input_idx):
        # Applying relu gave massive improvements over the linear version
        input_embeddings = F.relu(self.embeddings(input_idx))
        output = self.output_weights(input_embeddings)
        return output




def show_progress(iterable, total=None):
    ''' Readable wrapper for tqdm.  Accepts "total" kwarg only. '''
    return tqdm(
        iterable,
        desc="Batch",
        leave=False,
        total=total,
        mininterval=2)


class Trainer():
    ''' Largely similar to the word2vec notebook's version with adaptations '''

    def __init__(self, model, optimizer, device):

        self.device = device

        self.model = model
        self.optimizer = optimizer

        self.model.train()
        self.model.to(self.device)  # move model to GPU if available

    def train(self, train_dataset, output_folder, epochs=1):

        self.model.train()

        train_loss = 0.0
        for epoch in range(epochs):

            printerr("EPOCH", epoch, "started")

            epoch_loss = 0.0
            len_train = 0

            # each element (sample) in train_dataset is a batch
            for step, sample in show_progress( enumerate(train_dataset),
                                               total=len(dataset)//BATCH_SIZE):
                # inputs in the batch
                inputs = sample['inputs'].to(self.device)
                # outputs in the batch
                targets = sample['targets'].to(self.device)

                # make sure inputs are in perfect shape
                inputs = inputs.reshape(len(inputs), -1)

                output_distribution = self.model(inputs)
                # compute loss
                loss = self.model.loss_function(output_distribution, targets)
                loss.backward()
                # updates the parameters
                self.optimizer.step()
                self.optimizer.zero_grad()

                epoch_loss += loss.item()
                len_train += 1

            avg_epoch_loss = epoch_loss / len_train

            printerr('Epoch: {} avg loss = {:0.4f}'.format(epoch, avg_epoch_loss))

            train_loss += avg_epoch_loss

            # save model state (not used for exercise)
            #torch.save(self.model.state_dict(),
            #          os.path.join(
            #              output_folder, 'state_{}.pt'.format(epoch)))

        avg_epoch_loss = train_loss / epochs
        return avg_epoch_loss


class Evaluator():
    ''' OUT OF THE SCOPE OF THE EXERCISE
    Used during development to assess similarity of my predictions to the
    ground truth '''
    def __init__(self, model, device):
        self.model = model
        self.device = device

    def __call__(self, data):
        ''' Compute accuracy score. Input is in BATCHES i.e. comes from a DataLoader '''
        model.eval()
        correct, n = 0, 0
        for batch in data:
            # retrieve inputs and ground truth
            x = batch['inputs'].to(device)
            y_true = batch['targets'].to(device)
            # batch size may change when dataset is almost all consumed
            batch_size = y_true.shape[0]
            # compute prediction probabilities and reshape correctly
            y_pred = F.log_softmax(model(x), dim=2).reshape(batch_size,3)
            # get predicted class
            y_pred = torch.argmax(y_pred, axis=1)
            # count predictions that match with the ground truth
            correct += torch.sum(y_pred.eq(y_true)).item()
            # count elements processed so far
            n += batch_size
        return correct / n

def print_accuracy(evaluator, dataset, path=path):
    ''' Same as above, not used in this code but used during development '''
    printerr(f"computing score for {path}")
    acc = evaluator(dataset)
    printerr(f"Model accuracy on {path}:", acc)
    return acc


def run_test(model, valid_dataset):
    ''' Prints model's predictions on the test dataset '''
    model.eval()
    with torch.no_grad():
        for sample in valid_dataset:
            inputs = sample['inputs'].to(device)
            # Needed to know when to print \n
            islast = sample['islast']
            # Compute prediction
            model_out = F.softmax(model(inputs))
            # Map it to BIS encoding
            bis_out = id2bis[
                    torch.argmax(model_out).item()]
            # do the printing
            stdout.write(bis_out)
            if islast: stdout.write('\n')


# Hyperparameters
VOCAB_SIZE = 70
WINDOW_SIZE = 6 # 8 for "merged"
EMBEDDING_DIM = 8 # 20 for "merged"
EPOCHS = 6 # 9 for "merged"
LR = 0.001 # 0.005 for "merged"
BATCH_SIZE = 64

CHECKPOINT_FOLDER = './model_checkpoints/'

# Compute (character -> numerical id) dict based on input path
char2id = build_vocabulary(path, VOCAB_SIZE)

dataset = BonusExDataset(char2id, path, WINDOW_SIZE)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Instatiate my model, optimizer
model = BisModule(dataset.vocab_size,
    embedding_dim=EMBEDDING_DIM, window_size=WINDOW_SIZE)
optimizer = torch.optim.Adam(model.parameters(), lr=LR)

# Instantiation of trainer object
trainer = Trainer(model, optimizer, device)

# Instantiation of a dataloader to batch data automatically
dataloader = DataLoader(dataset, batch_size=BATCH_SIZE,
        pin_memory=True) # allows faster transfer to GPU memory

if __name__ == '__main__':

    ### TRAINING
    avg_loss = trainer.train(dataloader, CHECKPOINT_FOLDER, epochs=EPOCHS)

    ### LOADING
#    model.load_state_dict(torch.load(
#        os.path.join(CHECKPOINT_FOLDER, f'state_4.pt')))

    # Derive validation path
    validation_path = path[:path.rfind('.')] + '.test'

    # Instantiate validation dataset (using same char2id dict from training)
    validation_dataset = BonusExDataset(char2id, validation_path, WINDOW_SIZE,
            testing=True)

    # Print predictions to stdout
    run_test(model, validation_dataset)

    '''
    ### My other evaluation stuff
    if 'dev' in path:
        test_path = path.replace('dev','train')
    else:
        test_path = path.replace('train','dev')

    test_dataset = BonusExDataset(char2id, test_path, WINDOW_SIZE)
    test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE*2)

    evaluator = Evaluator(model, device)

    # Last epoch evaluation
    #print_accuracy(model, dataloader, path)
    #print_accuracy(model, test_dataloader, test_path)

    for i in [4,9,14]:
        printerr("computing accuracy for epoch", i)
        model.load_state_dict(torch.load(
            os.path.join(CHECKPOINT_FOLDER, f'state_{i}.pt')))
        print_accuracy(evaluator, dataloader, path)
        print_accuracy(evaluator, test_dataloader, test_path)
    '''
